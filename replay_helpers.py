import datetime
import configparser
import os, sys

class Config:
    def __init__(self, configPath):
        p = os.path.dirname(os.path.realpath(sys.argv[0])) + "/" + configPath
        if os.path.exists(p):
            parser = configparser.RawConfigParser()
            parser.read(p)
            self.playerName = parser.get("Player", "name")
            self.patch = parser.get("Game", "patch")
        else:
            print("No config file found at " + configPath)

def secondsForFrame(frame):
    return frame / 22.4

def timeStringToSeconds(time):
    time = str(time)
    if ":" in time:
        return int(time.split(":")[0]) * 60 + int(time.split(":")[1])
    else:
        return int(time)

def frameForSeconds(seconds):
    return seconds * 22.4

def timeFromSeconds(seconds):
    t = str(datetime.timedelta(seconds=int(seconds))).lstrip("0:")
    if len(t) == 2: t = "0:" + t
    elif len(t) == 1: t = "0:0" + t
    return t

def timeForFrame(frame):
    return timeFromSeconds(frame / 22.4)


def strip(player_name):
    s = player_name.replace("Player 1 - ", "")
    s = s.replace("Player 2 - ", "")
    s = s.replace(" (Zerg)", "")
    s = s.replace(" (Terran)", "")
    s = s.replace(" (Protoss)", "")
    return s

def matchup(replay, player_name):
    result = ""
    p1 = strip(str(replay.player[1]))
    if 2 in replay.player.keys():
        p2 = strip(str(replay.player[2]))
        result = (p1 + " vs " +  p2) if player_name in p1 else (p2 + " vs " +  p1)
    else:
        result = p1

    return result

def findZergPlayer(replay):
    for p in replay.player.values():
        if "zerg" in str(p).lower():
            return strip(str(p))

def raceForPlayer(replay, playerName):
    for p in replay.player.values():
        s = str(p)
        if playerName in s:
            return getRace(s)

def getRace(p):
    if "(Zerg)" in p:
        return "Z"
    elif "(Terran)" in p:
        return "T"
    elif "(Protoss)" in p:
        return "P"

def hasPlayer(replay, playerName):
    players = ""
    for k in replay.player.keys():
        players += str(replay.player[k])
    return playerName in players


def races(replay, playerName):
    if len(replay.player.keys()) > 1:
        r1 = getRace(str(replay.player[1]))
        r2 = getRace(str(replay.player[2]))

        # if the player is Zerg, show matchups as ZvP, ZvT, etc. Not TvZ or PvZ
        if playerName in str(replay.player[1]):
            return r1 + "v" + r2
        else:
            return r2 + "v" + r1

    else:
        return ""

def winLoss(replay, player_name):
    if str(replay.winner).find(player_name) != -1:
        return "win"
    else:
        return "loss"
