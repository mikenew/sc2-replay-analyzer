
#!/usr/bin/env python

import sc2reader
import os
import sys
import datetime
import traceback
from pprint import pprint
import argparse
import glob
from replay_helpers import *

replay = sc2reader.load_replay(sys.argv[1], load_level=2, load_map=True)
replay.load_map()
#print(replay.map)
print(replay.map.filename)
