To-Do
-----
* ✓ look into plotting (maybe this: https://github.com/nschloe/termplotlib)
* should check for creep tumors being spawned and adjust larva rates based on that
* ✓ may need to check object owners so things don't get messed up in ZvZ
* ✓ doesn't account for the first hatchery spawning with 3 larva, or other hatcheries starting with 1
* ✓ calculated larva rates should account for when queens come out
* ✓ figure out how to do a config file
  * ✓ store player name in config
* ✓ track worker production

* ✓ track injects
  * ggtracker does it like [this](https://github.com/dsjoerg/ggpyjobs/blob/249647fbe944799655ae6c842343af09ba23942a/sc2parse/plugins.py#L1123)
    but it seems buggy. Does it account for queued injects?
  * ✓ could maybe just look for when 3 larva spawn in a short span at the same hatchery and work backwards
    this wouldn't account for queued injects though
  * not sure if the SpawnLarva event will be reliable (it could probably be cancelled and we wouldn't know)

* ✓ track larva caps


Time
----
The times shown in the events needs to be multiplied by (42.86/60) to get real time values
OR, divide the frame time by 22.4, i.e. `print(event.frame / 22.4)`

**larva spawn rates:**
ideal larva spawn rate is: 1/(29/3) + 1/11 == 0.1943573667711599
or in terms of larva per second: 1/(1/(29/3) + 1/11) == 5.14516129032258
So the ideal time between larva spawns (on average) for a never-larva-capped-always-injected hatchery is about 5.15
That's discounting the fact that the first (and usually second) hatchery can't have a queen right away, 
so the real number will be slightly higher, depending on the length of the game


Objects
-------
looks like there's `replay.objects`, which is a list of units. 
Since each unit has a `finished_at` and `died_at` 
(unless it lived to the end of the game), it might be enough to just read from that list

there is also `replay.active_units`, but it seems like it's only a subset of the units in the game


Hatchery Spawning
-----------------
Seems like at the start of a game, there is a "unit born event" for a hatchery that looks like this:
```
00.00→ Player 1 - CareAgain (Zerg) - Unit born Hive [2D80001]
{'frame': 0, 'second': 0, 'name': 'UnitBornEvent', 'unit_id_index': 182, 'unit_id_recycle': 1, 'unit_id': 47710209, 'unit': Hive [2D80001], 'unit_type_name': 'HLightshade LE (254).SC2Replayatchery', 'control_pid': 1, 'upkeep_pid': 1, 'unit_upkeeper': Player 1 - CareAgain (Zerg), 'unit_controller': Player 1 - CareAgain (Zerg), 'x': 40, 'y': 131, 'location': (40, 131)}
```

weirdly, it's sometimes called a hive or a liar, even though it's a hatchery and the `unit_type_name` is hatchery.


When a hatchery is started, the event looks like this.
Doesn't seem like the the name is ever messed up the same way the "unit born" event is sometimes.
```
01.07→ Player 1 - CareAgain (Zerg) - Unit initiated Hatchery [3580002]{'frame': 1076, 'second': 67, 'name': 'UnitInitEvent', 'unit_id_index': 214, 'unit_id_recycle': 2, 'unit_id': 56098818, 'unit': Hatchery [3580002], 'unit_type_name': 'Hatchery', 'control_pid': 1, 'upkeep_pid': 1, 'unit_upkeeper': Player 1 - CareAgain (Zerg), 'unit_controller': Player 1 - CareAgain (Zerg), 'x': 38, 'y': 102, 'location': (38, 102)}
```

More importantly, there is a "done" event when a hatchery completes, but this will *not* be there for the first hatchery:
```
02.47→ Player 1 - CareAgain (Zerg) - Unit Hatchery [3580002] done{'frame': 2676, 'second': 167, 'name': 'UnitDoneEvent', 'unit_id_index': 214, 'unit_id_recycle': 2, 'unit_id': 56098818, 'unit': Hatchery [3580002]}
```

Units
-----
The `unit` object from the `UnitBornEvent` or `UnitDoneEvent` looks like this. 
The lifespan can be figured out by the `finished_at` and `died_at`, u
but if the hatchery is never kill then `died_at` will be `None`.
```
{'owner': Player 1 - CareAgain (Zerg), 'started_at': 7521, 'finished_at': 9121, 'died_at': 11206, 'killed_by': Player 2 - Noirtier (Protoss), 'killing_player': Player 2 - Noirtier (Protoss), 'killing_unit': VoidRay [5E80004], 'killed_units': [], 'id': 87818243, '_type_class': <sc2reader.data.UnitType object at 0x7f091a481c40>, 'type_history': OrderedDict([(7521, <sc2reader.data.UnitType object at 0x7f091a481c40>)]), 'hallucinated': False, 'flags': 1, 'location': (64, 110)}
```


Injects
-------
There's an event like this when spawnlarva is cast
```
03.41→CareAgain       Ability (E20) - SpawnLarva; Target: Hatchery [03580002]; Location: (38.5, 102.5, 40912){'pid': 0, 'player': Player 1 - CareAgain (Zerg), 'frame': 3546, 'second': 221, 'is_local': True, 'name': 'TargetUnitCommandEvent', 'flags': 256, 'flag': {'alternate': False, 'queued': False, 'preempt': False, 'smart_click': False, 'smart_rally': False, 'subgroup': False, 'set_autocast': False, 'set_autocast_on': False, 'user': True, 'data_a': False, 'data_passenger': False, 'data_b': False, 'data_abil_queue_order_id': False, 'ai': False, 'ai_ignore_on_finish': False, 'is_order': False, 'script': False, 'homogenous_interruption': False, 'minimap': False, 'repeat': False, 'dispatch_to_other_unit': False, 'target_self': False}, 'has_ability': True, 'ability_link': 113, 'command_index': 0, 'ability_data': None, 'ability_id': 3616, 'ability': <sc2reader.data.Ability object at 0x7f31e83c8ac0>, 'ability_name': 'SpawnLarva', 'ability_type': 'TargetUnit', 'ability_type_data': {'flags': 111, 'timer': 0, 'unit_tag': 56098818, 'unit_link': 109, 'control_player_id': 1, 'upkeep_player_id': 1, 'point': {'x': 157696, 'y': 419840, 'z': 40912}}, 'other_unit_id': None, 'other_unit': None, 'target_flags': 111, 'target_timer': 0, 'target_unit_id': 56098818, 'target_unit': None, 'target_unit_type': 109, 'control_player_id': 1, 'upkeep_player_id': 1, 'x': 38.5, 'y': 102.5, 'z': 40912, 'location': (38.5, 102.5, 40912), 'target': Hatchery [3580002]}
```

Larva
-----

Seems like the simplest, most consistent thing is looking for the "type changed to egg" 
event to track when larva is spent, 
and looking for "unit born larva" to track when it's created.

Technically, tracking when a larva is spent isn't needed. 
All we care about is how frequently a hatchery is spawning larva



Looks like this event is created when larva is born:
```
00.15→ Player 1 - CareAgain (Zerg) - Unit born Larva [3500001]
{'frame': 246, 'second': 15, 'name': 'UnitBornEvent', 'unit_id_index': 212, 'unit_id_recycle': 1, 'unit_id': 55574529, 'unit': Larva [3500001], 'unit_type_name': 'Larva', 'control_pid': 1, 'upkeep_pid': 1, 'unit_upkeeper': Player 1 - CareAgain (Zerg), 'unit_controller': Player 1 - CareAgain (Zerg), 'x': 40, 'y': 128, 'location': (40, 128)}
```

And this happens when it's changed to an egg
```
00.00→ Player 1 - CareAgain (Zerg) - Unit Larva [2DC0001] type changed to Egg{'frame': 6, 'second': 0, 'name': 'UnitTypeChangeEvent', 'unit_id_index': 183, 'unit_id_recycle': 1, 'unit_id': 47972353, 'unit': Larva [2DC0001], 'unit_type_name': 'Egg'}
```

At the same time, there's a morph event that shows what the larva is actually being turned into:
```
00.00	CareAgain       Ability (1860) - MorphDrone
{'pid': 0, 'player': Player 1 - CareAgain (Zerg), 'frame': 6, 'second': 0, 'is_local': True, 'name': 'BasicCommandEvent', 'flags': 131328, 'flag': {'alternate': False, 'queued': False, 'preempt': False, 'smart_click': False, 'smart_rally': False, 'subgroup': False, 'set_autocast': False, 'set_autocast_on': False, 'user': True, 'data_a': False, 'data_passenger': False, 'data_b': False, 'data_abil_queue_order_id': False, 'ai': False, 'ai_ignore_on_finish': False, 'is_order': False, 'script': False, 'homogenous_interruption': False, 'minimap': False, 'repeat': True, 'dispatch_to_other_unit': False, 'target_self': False}, 'has_ability': True, 'ability_link': 195, 'command_index': 0, 'ability_data': None, 'ability_id': 6240, 'ability': <sc2reader.data.Ability object at 0x7f31e833bdf0>, 'ability_name': 'MorphDrone', 'ability_type': 'None', 'ability_type_data': None, 'other_unit_id': None, 'other_unit': None}
```

Locations
---------
Looks like the origin of the map is bottom left, and structure locations are referenced by their centerpoint


Stats Events
------------
Every 10 seconds, plus once at the end of the game, there's a stats event that looks like this. `food_used` is supply.
```
28.6   00.40→  Player 1 - CareAgain (Zerg) - Stats Update
{'frame': 640, 'second': 40, 'name': 'PlayerStatsEvent', 'pid': 1, 'player': Player 1 - CareAgain (Zerg), 'stats': {0: 145, 1: 0, 2: 783, 3: 0, 4: 13, 5: 0, 6: 150, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 1100, 13: 0, 14: 0, 15: 0, 16: 0, 17: 0, 18: 0, 19: 0, 20: 0, 21: 0, 22: 0, 23: 0, 24: 0, 25: 0, 26: 0, 27: 0, 28: 0, 29: 57344, 30: 57344, 31: 0, 32: 0, 33: 0, 34: 0, 35: 0, 36: 0, 37: 0, 38: 0}, 'minerals_current': 145, 'vespene_current': 0, 'minerals_collection_rate': 783, 'vespene_collection_rate': 0, 'workers_active_count': 13, 'minerals_used_in_progress_army': 0, 'minerals_used_in_progress_economy': 150, 'minerals_used_in_progress_technology': 0, 'minerals_used_in_progress': 150, 'vespene_used_in_progress_army': 0, 'vespene_used_in_progress_economy': 0, 'vespene_used_in_progress_technology': 0, 'vespene_used_in_progress': 0, 'resources_used_in_progress': 150, 'minerals_used_current_army': 0, 'minerals_used_current_economy': 1100, 'minerals_used_current_technology': 0, 'minerals_used_current': 1100, 'vespene_used_current_army': 0, 'vespene_used_current_economy': 0, 'vespene_used_current_technology': 0, 'vespene_used_current': 0, 'resources_used_current': 1100, 'minerals_lost_army': 0, 'minerals_lost_economy': 0, 'minerals_lost_technology': 0, 'minerals_lost': 0, 'vespene_lost_army': 0, 'vespene_lost_economy': 0, 'vespene_lost_technology': 0, 'vespene_lost': 0, 'resources_lost': 0, 'minerals_killed_army': 0, 'minerals_killed_economy': 0, 'minerals_killed_technology': 0, 'minerals_killed': 0, 'vespene_killed_army': 0, 'vespene_killed_economy': 0, 'vespene_killed_technology': 0, 'vespene_killed': 0, 'resources_killed': 0, 'food_used': 14.0, 'food_made': 14.0, 'minerals_used_active_forces': 0, 'vespene_used_active_forces': 0, 'ff_minerals_lost_army': 0, 'ff_minerals_lost_economy': 0, 'ff_minerals_lost_technology': 0, 'ff_vespene_lost_army': 0, 'ff_vespene_lost_economy': 0, 'ff_vespene_lost_technology': 0}
```
