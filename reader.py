#!/usr/bin/env python

import sc2reader
import os
import sys
import datetime
import traceback
from pprint import pprint
import argparse
import glob
from replay_helpers import *

def strip(player_name):
    s = player_name.replace("Player 1 - ", "")
    s = s.replace("Player 2 - ", "")
    s = s.replace(" (Zerg)", "")
    s = s.replace(" (Terran)", "")
    s = s.replace(" (Protoss)", "")
    return s

def playerString(replay, playerName):
    result = ""
    p1 = strip(str(replay.player[1]))
    if 2 in replay.player.keys():
        p2 = strip(str(replay.player[2]))
        if playerName in (p1 + p2):
            result = (p1 + p2).replace(playerName, "")
        else:
            result = (p1 + " vs. " + p2)
    else:
        result = p1
    return result

def gameLength(replay):
    return str(replay.game_length).replace(".", ":")

def gameDate(replay):
    return str(replay.end_time)

def isCurrentRelease(replay, release):
    return (release in replay.release_string)

def findOldReplays(path, release, recursive=False, teams=False, invert=False):
    p = path + '/**/*.SC2Replay' if recursive else path + '**/*.SC2Replay'
    for file in sorted(glob.iglob(p, recursive=recursive), key=os.path.getmtime):
        try:
            replay = sc2reader.load_replay(file, load_level=2)
            current = isCurrentRelease(replay, release)
            if current and invert:
                print(file)
            elif not current and not invert:
                print(file)

        except Exception:
            traceback.print_exc()
            print("error reading replay at " + file)

def readReplays(path, playerName, patch, recursive=False, teams=False, currentPatchOnly=False):
    p = path + '/**/*.SC2Replay' if recursive else path + '**/*.SC2Replay'
    for file in sorted(glob.iglob(p, recursive=recursive), key=os.path.getmtime):
        try:
            replay = sc2reader.load_replay(file, load_level=2)
            if len(replay.humans) > 1 and (('1v1' in replay.real_type) or teams):
                p = playerString(replay, playerName)
                l = gameLength(replay)
                d = gameDate(replay)
                w = winLoss(replay, playerName)
                m = matchup(replay, playerName)
                r = races(replay, playerName)
                rl = isCurrentRelease(replay, patch)

                # usernames never seem to be longer than 23 characters
                if hasPlayer(replay, playerName):
                    result = '{0: <24}'.format(p) + " | " + '{0: <4}'.format(w) + " | " + l + " | " + r + " | " + d + " | " + os.path.basename(file)
                else:
                    result = '{0: <24}'.format(p) + " | " + l + " | " + r + " | " + d + " | " + os.path.basename(file)

                print(result)
                #pprint(vars(replay))

        except Exception:
            traceback.print_exc()
            print("error reading replay at " + file)

if __name__ == '__main__':
    config = Config("config.txt")
    if not hasattr(config, 'playerName'): quit()

    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", type=str, dest="replay_dir",
            default="/home/mikenew/syncthing/sc2_replays",
        help="path to search for replays", metavar="FILE")
    parser.add_argument("-r", "--recursive", action='store_true', dest="recursive", help="search subdirectories (off by default)")
    parser.add_argument("-t", "--teams", action='store_true', dest="team_games", help="include team games in output (off by default)")
    parser.add_argument("--oldpatch", action='store_true', dest="old_patch", help="print a list of replays that are on the old patch. Useful for archiving. Make sure the patch string is up to date in the config")
    parser.add_argument("--newpatch", action='store_true', dest="new_patch", help="print a list of replays that are on the current patch.")
    args = parser.parse_args()

    if args.old_patch:
        findOldReplays(args.replay_dir, config.patch, args.recursive, args.team_games)
    elif args.new_patch:
        findOldReplays(args.replay_dir, config.patch, args.recursive, args.team_games, invert = True)
    else:
        readReplays(args.replay_dir, config.playerName, config.patch, args.recursive, args.team_games)

    quit()
