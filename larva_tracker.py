#!/usr/bin/env python

import sc2reader # docs:             https://sc2reader.readthedocs.io/en/latest
                 # ggtracker source: https://github.com/dsjoerg/ggpyjobs
from pprint import pprint
import sys, os
from colorama import Fore, Style
from enum import Enum
import asciichartpy

from replay_helpers import *

#Contants
idealLarvaSpawn = (1/(29/3) + 1/11)
baseLarvaSpawn = 1/11
injectLarvaSpawn = 1/(29/3)

def lookupBuildTime(unitName):
    n = unitName.lower()

    if n == "queen":
        return 36
    elif n == "drone":
        return 12
    elif n == "roach":
        return 18
    elif n == "larva":
        return 0
    elif n == "lair":
        return 57
    elif n == "hive":
        return 71
    else:
        print("build time not found for: " + n)
        return 0

class BaseUnit:
    def __init__(self, unit, replay):
        self.unit = unit
        self.id = unit.id
        self.endFrame = replay.frames
        self.startFrame = 0
        self.beginBuildingFrame = 0
        self.name = unit.name
        self.wasBorn = False

        # this is super hacky. tech structures like the lair don't have a start or end frame,
        # they just have an 'OrderedDict' containing the frame they were morphed
        #if tech and (self.name.lower() == "lair" or self.name.lower() == "hive") and self.startFrame < 1:
        #  self.startFrame = list(unit.type_history.keys())[1]

        if unit.finished_at is not None: 
            if self.startFrame < 1: self.startFrame = unit.finished_at
            self.neverFinished = False
        else: 
            self.neverFinished = True

        if self.startFrame <= 0: self.wasBorn = True


        if not self.wasBorn:
            if unit.started_at is not None: 
                if unit.finished_at is not None:
                    if unit.started_at < unit.finished_at:
                        self.beginBuildingFrame = unit.started_at

            if self.beginBuildingFrame <= 0:
                self.beginBuildingFrame = self.startFrame - frameForSeconds(lookupBuildTime(unit.name))

        if unit.died_at is not None: self.endFrame = unit.died_at

        self.x = unit.location[0]
        self.y = unit.location[1]

class Hatchery(BaseUnit):
    def __init__(self, unit, replay):
        super().__init__(unit, replay)

        self.queenFrame = 0
        self.isFirstHatchery = False

        if unit.finished_at is not None: 
            self.queenFrame = self.startFrame

        self.larva = []
        self.number = -1 # the order that hatcheries are build shouuld be set externally

    def larvaCount(self, atFrame=-1):
        if atFrame >= 0:
            larvaCount = 0
            for l in self.larva:
                if l.startFrame <= atFrame: larvaCount += 1
            return larvaCount
        else:
            return len(self.larva)

    def larvaOfType(self, type):
        filtered = []
        for l in self.larva:
            if l.type == type: filtered.append(l)
        return filtered

    def larvaCapTime(self, atFrame=-1):
        if atFrame < 0: atFrame = sys.maxsize

        baseOnly = self.larvaOfType(LarvaType.BASE)
        delayedFrames = 0

        for i in range(len(baseOnly)):
            l = baseOnly[i]
            if l.startFrame <= atFrame:
                # 240 frames is the standard spawn time for a larva
                # if the frame time between larva is longer, it means the hatchery was larva blocked
                frameGap = 240 
                if i == 0:
                    frameGap = l.startFrame - self.larva[0].startFrame
                else:
                    frameGap = l.startFrame - baseOnly[i-1].startFrame
                delayedFrames += (frameGap - 240)
        return delayedFrames

    def larvaSpawnRate(self, atFrame=-1):
        frames = self.totalFrames(atFrame)

        larvaCount = len(self.larva)
        if atFrame >= 0:
            larvaCount = 0
            for l in self.larva:
                if l.startFrame <= atFrame: larvaCount += 1

        avgFrames = frames / max(larvaCount, 1) # min 1 so we don't divide by zero

        return round(avgFrames / 22.4, 1)

    def idealLarvaSpawnRate(self, atFrame=-1):
        frames = self.totalFrames(atFrame)
        avgFrames = frames / max(self.idealLarvaCount(atFrame), 1) # min 1 so we don't divide by zero

        return round(avgFrames / 22.4, 1)

    def idealLarvaCount(self, atFrame=-1):
        startCount = 3 if self.number == 0 else 1 # the first hatchery spawns with 3 larva. Others spawn with 1
        injectCount = (self.queenFrames(atFrame) / 22.4) * injectLarvaSpawn
        baseCount = (self.totalFrames(atFrame) / 22.4) * baseLarvaSpawn

        return int(baseCount) + int(injectCount) + startCount

    #number of frames that the hatchery had a queen available
    def queenFrames(self, atFrame=-1):
        if atFrame < 0: atFrame = self.endFrame
        start = max(self.queenFrame, self.startFrame)
        return max(atFrame - start, 0)

    def totalFrames(self, atFrame=-1):
        if atFrame < 0: atFrame = self.endFrame
        return max(atFrame - self.startFrame, 0)

    def lifeTime(self, atFrame=-1):
        return round(self.totalFrames(atFrame) / 22.4, 1)

    def __str__(self):
        return ("Hatchery " + str(self.id) + " lived: " + str(self.lifeTime()) + 
                " at: " + str(self.x) + ", " + str(self.y) + " larva count: " + str(len(self.larva)))

class LarvaType(Enum):
    BORN = 1
    INJECT = 2
    BASE = 3

class Larva(BaseUnit):
    def __init__(self, unit, replay):
        super().__init__(unit, replay)
        self.spent = len(unit.type_history) > 1
        #if unit.died_at is not None and unit.killing_player is None: self.spent = True
        self.hatcheryTag = -1
        self.type = LarvaType.BASE

    def spawnTime(self):
        return round(self.startFrame / 22.4, 1)

    def findHatchery(self, hatcheries):
        result = None
        for h in hatcheries:
            if ((self.x >= h.x - 2 and self.x <= h.x + 2) and 
                    (self.y >= h.y - 4 and self.y <= h.y - 2) and
                    (self.startFrame >= h.startFrame and self.startFrame <= h.endFrame)):
                if result is not None:
                    print("found duplicate match")
                result = h

        if result is None:
            print("failed matching larva to hatchery: " + str(self))
        return result

class GameState:
    def __init__(self, replay, playerName):
        self.hatcheries = []
        self.larva = []
        self.queens = []
        self.workers = []
        self.roaches = [] # there needs to be a more generic way for this
        self.techStructures = []
        self.upgradeEvents = []
        self.maxOutFrame = 0
        self.playerName = playerName
        self.replay = replay

        self.resourceFloat = []
        #self.averageResourceFloatUntilMax = sum(self.resourceFloat) / len(self.resourceFloat)

        self.loadEvents()

    def resourceFloatValues(self, atFrame=-1):
        floats = []
        for t in self.resourceFloat:
            if atFrame >= 0:
                if t[1] <= atFrame:
                    floats.append(t[0])
            else:
                floats.append(t[0])
        return floats

    def averageResourceFloat(self, atFrame=-1):
        i = 0
        total = 0
        for t in self.resourceFloat:
            if atFrame >= 0:
                if t[1] <= atFrame:
                    total += t[0]
                    i += 1
            else:
                total += t[0]
                i += 1
        return int(total / i)

    def loadEvents(self):
        for e in self.replay.events:
            if e.name == "PlayerStatsEvent" and self.playerName in str(e.player):
                self.resourceFloat.append((e.minerals_current + e.vespene_current, e.frame))

                if e.food_used >= 198 and self.maxOutFrame == 0:
                    self.maxOutFrame = e.frame

        self.upgradeEvents = list(filter(lambda x: x.name == "UpgradeCompleteEvent" and 
                                         self.playerName in str(x.player), self.replay.events))
        #for u in self.upgradeEvents: print(u)

        for unit in self.replay.objects.values():
            if "lair" in unit.name.lower(): print(unit.name)

            if unit.owner is not None and self.playerName in str(unit.owner):
                n = unit.name.lower()

                # this might be bugged. Lair and Hive might be considered separate structures this way
                if (n == "hatchery" or n == "lair" or n == "hive"):
                    h = Hatchery(unit, self.replay)
                    if not h.neverFinished: self.hatcheries.append(h)
                    #if n == "lair" or n == "hive": 
                    #  self.techStructures.append(BaseUnit(unit, self.replay, True))
                elif n == "larva":
                    self.larva.append(Larva(unit, self.replay))
                elif n == "queen":
                    self.queens.append(BaseUnit(unit, self.replay))
                elif n == "drone":
                    self.workers.append(BaseUnit(unit, self.replay))
                elif n == "roach":
                    self.roaches.append(BaseUnit(unit, self.replay))
                elif n == "spawningpool" or n == "banelingnest":
                    self.techStructures.append(BaseUnit(unit, self.replay))
                #else: print(unit)

        self.hatcheries.sort(key=lambda s: s.startFrame)
        self.queens.sort(key=lambda s: s.startFrame)
        self.techStructures.sort(key=lambda s: s.startFrame)

        for i in range(len(self.hatcheries)):
            self.hatcheries[i].number = i

        for i in range(len(self.queens)):
            q = self.queens[i]
            if i < len(self.hatcheries):
                self.hatcheries[i].queenFrame = q.startFrame

        for l in self.larva: 
            h = l.findHatchery(self.hatcheries)
            h.larva.append(l)
            l.hatcheryTag = h.number

        # look through the larva for each hatchery categorize larva as BORN (spawned with the hatchery), 
        # INJECTED (came from an inject) or base (spawned naturally by the hatchery)
        # look for clusters of 3 larva that spawn within 8 frames to mark them as inject
        for h in self.hatcheries:
            for i in range(len(h.larva)):
                l = h.larva[i]

                # the first hatchery starts with 3 larva, the others start with 1
                if h.number == 0:
                    if i < 3: l.type = LarvaType.BORN
                else:
                    if i == 0: l.type = LarvaType.BORN

                # skip larva that were already marked as from and inject
                if i > 0 and i + 1 < len(h.larva) and l.type == LarvaType.BASE:
                    previous =  h.larva[i-1]
                    next =  h.larva[i+1]

                    if previous.startFrame + 4 >= l.startFrame and next.startFrame - 4 <= l.startFrame:
                        previous.type = LarvaType.INJECT
                        l.type = LarvaType.INJECT
                        next.type = LarvaType.INJECT

    def metaData(self):
        return (races(self.replay, self.playerName) + " - " + 
                matchup(self.replay, self.playerName) + 
                " (" + winLoss(self.replay, self.playerName).capitalize() + ")")

    def larvaStats(self):
        idealTotal = 0
        spawned = 0
        frame = self.maxOutFrame if self.maxOutFrame > 0 else -1

        s =  "Hatchery #     lifetime    rate  (ideal)     total  (ideal)  capped    efficiency\n"
        s += "-------------------------------------------------------------------------------\n"

        for h in self.hatcheries: 
            idealTotal += h.idealLarvaCount(frame)
            spawned += h.larvaCount(frame)

            s += ("Hatchery " + str(h.number + 1) + 
                    ("" if (h.number + 1 > 9) else " ") + # pad single digits
                    "    {0: <7}".format(timeFromSeconds(h.lifeTime(frame))) + 
                    "     {0: <5}".format(h.larvaSpawnRate(frame)) + 
                    " (" + str(h.idealLarvaSpawnRate(frame)) + ")" + 
                    "       {0: <4}".format(h.larvaCount(frame)) + 
                    "   ({0: <5}".format(str(h.idealLarvaCount(frame)) + ")") + 
                    "   {0: <5}".format(str(timeForFrame(h.larvaCapTime(frame)))) + 
                    "     " + str(round(h.larvaCount(frame)/h.idealLarvaCount(frame) * 100, 1)) + "%\n")

        s += "\n"
        endPoint = "at max out (" + timeForFrame(frame) if frame > 0 else "at game end (" + timeForFrame(self.replay.frames)

        s += (endPoint + 
                "), spawned " + str(spawned) + " larva out of a possible " +
                str(idealTotal) + " (" + 
                str(round(spawned/idealTotal * 100, 1)) + "% efficiency)")

        return s

    def buildTimeString(self, time, ideal, grace=3):
       if int(time) <= (ideal + grace):
           return Fore.GREEN + timeFromSeconds(time) + Style.RESET_ALL + " (" + timeFromSeconds(ideal) + ")"
       else:
           return timeFromSeconds(time) + " (" + timeFromSeconds(ideal) + ")"

    def getStartTime(self, unitList, number=0, ideal="190", grace=2):
        # TODO: this doesn't account for killed units or morphed buildings. 
        # So "60th unit" doesn't mean there's actually 60 drones alive + in production
        # FIXME: this doesn't seem to be getting the time right for the 16 roach build.
        # looks like it's only counting them when they're out of production maybe?
        ideal = timeStringToSeconds(ideal)
        for i in range(len(unitList)):
            if i >= number:
                unit = unitList[i]

                # Don't include dead units (or drones that were used for buildings)
                alive = i
                for ii in range(min(number, len(unitList))):
                    if unitList[ii].endFrame <= unit.beginBuildingFrame:
                        alive -= 1

                if alive >= number:
                    t = secondsForFrame(unit.beginBuildingFrame)
                    return self.buildTimeString(t, ideal, grace)
        return "N/A"

    def getTechStartTime(self, name, ideal="190", grace=3):
        ideal = timeStringToSeconds(ideal)
        for s in self.techStructures:
            if s.name.lower() == name.lower():
                t = secondsForFrame(s.beginBuildingFrame)
                return self.buildTimeString(t, ideal, grace)
        return "N/A"

    def getUpgradeStartTime(self, name, time, ideal="190", grace=3):
        #ZergMissileWeaponsLevel1 - 114
        #GlialReconstitution      - 79
        #CentrificalHooks         - ??
        ideal = timeStringToSeconds(ideal)
        for s in self.upgradeEvents:
          #{'frame': 8377, 'second': 523, 'name': 'UpgradeCompleteEvent', 'pid': 1, 'player': Player 1 - Batman (Zerg), 'upgrade_type_name': 'SprayZerg', 'count': 1}
            if s.upgrade_type_name == name:
                t = secondsForFrame(s.frame - (time * 22.4))
                return self.buildTimeString(t, ideal, grace)
        return "N/A"

    def spawningPoolStartTime(self, ideal="72", grace=2):
        return self.getTechStartTime("SpawningPool", ideal, grace)

    def queenStartTime(self, number=0, ideal="119", grace=2):
        return self.getStartTime(self.queens, number, ideal, grace)

    def droneStartTime(self, number=0, ideal="190", grace=2):
        return self.getStartTime(self.workers, number, ideal, grace)

    def roachStartTime(self, number=0, ideal="190", grace=2):
        return self.getStartTime(self.roaches, number, ideal, grace)

    def hatcheryStartTime(self, number=0, ideal="48", grace=2):
        return self.getStartTime(self.hatcheries, number, ideal, grace)

    def getHatcheryMorphFrame(self, number=1):
        startFrame = 0
        for h in self.hatcheries:
            # tech structures like the lair don't have a start or end frame,
            # they just have an 'OrderedDict' containing the frame they finished morphing
            if number < len(h.unit.type_history.keys()):
                startFrame = list(h.unit.type_history.keys())[number]
            if startFrame > 0:
                t = secondsForFrame(startFrame)
                return t
        return 0


    def lairStartTime(self, ideal="48", grace=2):
        t = self.getHatcheryMorphFrame(1)
        t = t - lookupBuildTime("lair")
        if t > 0:
            ideal = timeStringToSeconds(ideal)
            return self.buildTimeString(t, ideal, grace)
        else:
            return "N/A"

    def hiveStartTime(self, ideal="48", grace=2):
        t = self.getHatcheryMorphFrame(2)
        t = t - lookupBuildTime("hive")
        if t > 0:
            ideal = timeStringToSeconds(ideal)
            return self.buildTimeString(t, ideal, grace)
        else:
            return "N/A"


    def debugLog(self):
        pprint(vars(self.replay))


if __name__ == '__main__':
    replay = None
    config = Config("config.txt")
    if not hasattr(config, 'playerName'): quit()

    if len(sys.argv) > 1:
        if not sys.argv[1].endswith("SC2Replay"): 
            quit() # don't try and parse anything that isn't an SC2Replay
        replay = sc2reader.load_replay(sys.argv[1], load_map=False)
        print("")
        print("Analyzing " + config.playerName + " in " + os.path.basename(sys.argv[1]))
    else:
        print(Fore.RED + "provide a path to a replay")
        quit()

    if not hasPlayer(replay, config.playerName):
        player = findZergPlayer(replay)
        print(Fore.RED + "no player named " + config.playerName + ", showing stats for " + str(player))
        config.playerName = player
        #quit()

    game = GameState(replay, config.playerName)
    m = races(replay, config.playerName).lower()
    #game.debugLog()

    print("")
    print(Fore.BLUE + "Game Summary" + Style.RESET_ALL)
    print(Fore.BLUE + "------------" + Style.RESET_ALL)
    print(game.metaData())

    if raceForPlayer(replay, config.playerName) == "Z":
        print("")
        print(Fore.BLUE + "Larva Production" + Style.RESET_ALL)
        print(Fore.BLUE + "----------------" + Style.RESET_ALL)
        print(game.larvaStats())

    print("")
    print(Fore.BLUE + "Benchmarks" + Style.RESET_ALL)
    print(Fore.BLUE + "----------" + Style.RESET_ALL)
    print("second hatchery   " + game.hatcheryStartTime(1))
    print("spawning pool     " + game.spawningPoolStartTime(grace=1))
    print("first queen       " + game.queenStartTime(0))
    print("second queen      " + game.queenStartTime(1))
    print("third hatchery    " + game.hatcheryStartTime(2, "2:19", grace=10))
    print("5th queen         " + game.queenStartTime(4, "3:36", grace=0))
    print("")
    print("35th drone        " + game.droneStartTime(35, "3:12"))
    print("43rd drone        " + game.droneStartTime(43, "3:47"))
    print("6th queen         " + game.queenStartTime(5, "3:47"))
    print("")

    print(Fore.BLUE + "Primary Targets" + Style.RESET_ALL)
    print(Fore.BLUE + "---------------" + Style.RESET_ALL)

    if "zvt" in m or "tvz" in m:
        print("lair              " + game.lairStartTime("4:32"))
        print("90th drone        " + game.droneStartTime(90, "7:45"))

    elif "zvz" in m:
        print("37th drone        " + game.droneStartTime(37, "3:32"))
        print("16th roach        " + game.roachStartTime(16, "4:36"))
        print("+1 range          " + game.getUpgradeStartTime("ZergMissileWeaponsLevel1", 114, "3:08"))
        print("roach speed       " + game.getUpgradeStartTime("GlialReconstitution", 79, "3:37"))

    elif "zvp" in m or "pvz" in m:
        print("57th drone        " + game.droneStartTime(57, "4:48"))
        print("+1 melee          " + game.getUpgradeStartTime("ZergMeleeWeaponsLevel1", 114, "3:40"))
        print("bane nest         " + game.getTechStartTime("BanelingNest","4:00"))
        print("lair              " + game.lairStartTime("4:32"))
        print("+2 melee          " + game.getUpgradeStartTime("ZergMeleeWeaponsLevel2", 136, "5:34"))

    print("")

    print(Fore.BLUE + "Lategame" + Style.RESET_ALL)
    print(Fore.BLUE + "--------" + Style.RESET_ALL)
    print("hive              " + game.hiveStartTime("9:00"))
    print("+3 range          " + game.getUpgradeStartTime("ZergMeleeWeaponsLevel3", 157, "5:34"))
    print("+3 carapace       " + game.getUpgradeStartTime("ZergCarapaceLevel3", 157, "5:34"))

    print("")

    if game.maxOutFrame > 0:
        print(Fore.BLUE + "Resource Float Before Max Out" + Style.RESET_ALL)
        print(Fore.BLUE + "-----------------------------" + Style.RESET_ALL)
        print(str(int(game.averageResourceFloat(game.maxOutFrame))) + " average unspent resources before max (" + timeForFrame(game.maxOutFrame) + ")")
        print("")
        print(asciichartpy.plot(game.resourceFloatValues(game.maxOutFrame), {'height':15}))
        print("")
    else:
        print(Fore.BLUE + "Resource Float Before Game End" + Style.RESET_ALL)
        print(Fore.BLUE + "------------------------------" + Style.RESET_ALL)
        print(str(int(game.averageResourceFloat())) + " average unspent resources before game end (" + timeForFrame(replay.frames) + ")")
        print("")
        print(asciichartpy.plot(game.resourceFloatValues(), {'height':15}))
        print("")
