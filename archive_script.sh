#!/bin/sh
OIFS="$IFS"
IFS=$'\n'
for file in $(./reader.py -p $1 --oldpatch)  
do
     echo $file
     mv $file $2
done
IFS="$OIFS"
