#!/usr/bin/env python

from larva_tracker import *
import os

if __name__ == '__main__':
    replay = None
    config = Config("config.txt")
    if not hasattr(config, 'playerName'): quit()

    if len(sys.argv) > 1:
        totals = []
        maxOuts = []

        for filename in os.listdir(sys.argv[1]):
            f = os.path.join(sys.argv[1], filename)
            if f.endswith("SC2Replay"):
                print(f)
                replay = sc2reader.load_replay(f, load_map=False)

                if not hasPlayer(replay, config.playerName):
                    player = findZergPlayer(replay)
                    print(Fore.RED + "no player named " + config.playerName + ", showing stats for " + player + Style.RESET_ALL)
                    config.playerName = player

                game = GameState(replay, config.playerName)

                # 22.4 frames per second, i.e. seconds * 22.4

                #frame = 5645  # 4:12
                #frame = 9946  # 7:24
                #frame = 12096 # 9:00
                #frame = 20160 # 15:00
                if game.maxOutFrame > 0:
                    totals.append(game.averageResourceFloat(game.maxOutFrame))
                    maxOuts.append(game.maxOutFrame)

        #print(config.playerName + ": average resource float across " 
        #        + str(len(totals)) + " games at " 
        #        + timeForFrame(frame) + " -> " + str(int(sum(totals) / len(totals))))

        print(config.playerName + ": average resource float across " 
                + str(len(totals)) + " games at " 
                + "max out -> " + str(int(sum(totals) / len(totals))))

        print(config.playerName + ": average max out time across " 
                + str(len(maxOuts)) + " games -> " + timeForFrame(int(sum(maxOuts) / len(maxOuts))))

    else:
        print(Fore.RED + "provide a path to a replay" + Style.RESET_ALL)
        quit()
